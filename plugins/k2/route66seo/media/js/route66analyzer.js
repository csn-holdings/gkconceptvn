Route66UrlAnalyzer = window.Route66Seo || {};

(function(Route66UrlAnalyzer, $) {
  'use strict';

  Route66UrlAnalyzer.start = function() {

    // Options
    this.options = Joomla.getOptions('Route66UrlAnalyzerOptions');

    // Analyzer
    this.analyzer = new Route66Seo(this.options);

    // Elements
    this.$form = $('#adminForm');
    this.$keywordField = $('#jform_route66seo_keyword');
    this.$urlField = $('#jform_route66seo_url');

    // Values
    this.text = '';
    this.attributes = {
      keyword: '',
      synonyms: '',
      description: '',
      title: '',
      url: '',
      permalink: ''
    };

    // Add events
    this.addEvents();
  };

  Route66UrlAnalyzer.addEvents = function() {
    this.$keywordField.on('change', $.proxy(this.analyze, this));
  };

  Route66UrlAnalyzer.analyze = function() {
    this.attributes.keyword = this.$keywordField.val();
    this.analyzer.analyze(this.text, this.attributes);
  };

  Route66UrlAnalyzer.fetchPage = function(text) {

    var url = this.$urlField.val();

    if (!this.isValidUrl(url)) {
      Joomla.renderMessages({
        "error": [Joomla.JText._('COM_ROUTE66_YOU_CAN_USE_ONLY_INTERNAL_URLS')]
      });
      return;
    }
    Joomla.removeMessages();

    $.get(url).done($.proxy(function(response) {

      if (response) {
        var parser = new DOMParser();
        var doc = parser.parseFromString(response, 'text/html');

        if (doc) {

          var titleEl = doc.getElementsByTagName('title');
          if (titleEl.length) {
            this.attributes.title = titleEl[0].innerHTML;
          }

          var description = '';
          $.each(doc.getElementsByTagName('meta'), function(index, node) {
            if (node.getAttribute('name') == 'description') {
              description = node.getAttribute('content');
            }
          });
          this.attributes.description = description;

          var bodyEl = doc.getElementsByTagName('body');
          if (bodyEl.length) {
            this.text = bodyEl[0].innerHTML;
          }
        }
      }
      this.attributes.permalink = url;
      this.attributes.url = url.substr(this.options.site.length);
      this.analyze();
    }, this)).fail(function(error) {
      Joomla.renderMessages({
        error: [error.statusText]
      });
    });
  };

  Route66UrlAnalyzer.isValidUrl = function(url) {
    if (url.indexOf('http://') !== 0 && url.indexOf('https://') !== 0) {
      return false;
    }
    var parser = document.createElement('a');
    parser.href = url;
    return parser.protocol == window.location.protocol && parser.host == window.location.host;
  };

}(Route66UrlAnalyzer, jQuery));
