<?php
/*------------------------------------------------------------------------
# plg_zoho_salesiq - Zoho SalesIQ
# ------------------------------------------------------------------------
# author    SalesIQ Team
# copyright Copyright (C) 2014 zoho.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: https://salesiq.zoho.com
# Technical Support:  https://www.zoho.com/salesiq/help/
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.plugin.plugin');

class plgSystemZoho_SalesIQ extends JPlugin {

	function __construct(&$subject, $config) {
		parent::__construct($subject, $config);
	}

	function onAfterRender() {
		$app = JFactory::getApplication();

		$format = JRequest::getCmd('format');
		$tmpl = JRequest::getCmd('tmpl');

		if ($format == 'raw' || $tmpl == 'component') {
			return;
		}

		$widget_code = $this->params->get('salesiq_widget_code', '');
		$salesiq_show = $this->params->get('salesiq_show', '0');

		if ($widget_code == '' || $salesiq_show == 1 || $app->isAdmin()) {
			return;
		}

		$mobile = $this->params->get('salesiq_mobile', 0);
		$tablet = $this->params->get('salesiq_tablet', 0);

		if ($tablet || $mobile) {
      if (!class_exists('Mobile_Detect')) {
			  require_once(dirname(__FILE__) . '/zoho_salesiq/mobile_detect/Mobile_Detect.php');
      }

			$detect = new Mobile_Detect();
			if ($tablet && $detect->IsTablet()) {
				return;
      }
			if ($mobile && $detect->isMobile() && !$detect->isTablet()) {
				return;
      }
		}

		$buffer = JResponse::getBody();
		$buffer = str_replace("</body>", $widget_code . "</body>", $buffer);
		JResponse::setBody($buffer);
		return true;
	}

}

