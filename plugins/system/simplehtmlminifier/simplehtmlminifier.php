<?php

defined('_JEXEC') or die('Restricted Access');

/* Simple HTML Minifier extension for Joomla!
--------------------------------------------------------------
 Copyright (C) 2016 AddonDev. All rights reserved.
 Website: https://addondev.com
 GitHub: https://github.com/addondev
 Developer: Philip Sorokin
 Location: Russia, Moscow
 E-mail: philip.sorokin@gmail.com
 Created: January 2016
 License: GNU GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
--------------------------------------------------------------- */

class PlgSystemSimplehtmlminifier extends JPlugin
{
	private $_scriptExecutionTime = false;
	private $_exclusions = array();
	
	private $_blocks = array();
	private $_elements = array();
	
	private $_bl_placeholder = null;
	private $_el_placeholder = null;
	
	
	PUBLIC FUNCTION __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage('', JPATH_ADMINISTRATOR);
	}
	
	
	PUBLIC FUNCTION onAfterRender()
	{	
		$app = JFactory::getApplication();
		
		if($app->isSite() && !class_exists('JEditor', false))
		{
			if($this->_scriptExecutionTime = $this->params->get('script_execution_time'))
			{
				$this->_countScriptExecutionTime();
			}
			
			$html = $app->getBody();
			
			if($this->params->get('compress_html'))
			{
				if($input = $this->params->get('exclude_tags'))
				{
					$this->_exclusions[] = $input;
				}
				
				if($this->params->get('save_inline_scripts', true))
				{
					$this->_exclusions[] = array("<script(?:(?!\>|\ssrc).)*>", "</script\s*>");
				}
				
				if($this->params->get('use_excluding_comments'))
				{
					$this->_exclusions[] = array('<!--[NOCOMPRESS]-->', '<!--[ENDNOCOMPRESS]-->', true);
				}
				
				$html = $this->_compressHtml($html);
			}
			
			if($this->params->get('remove_html_comments'))
			{
				$html = $this->_removeHtmlComments($html);
			}
			
			if($this->_scriptExecutionTime)
			{
				$html = $this->_countScriptExecutionTime($html);
			}
			
			$app->setBody($html);
		
		}
		
	}
	
	
	PRIVATE FUNCTION _removeHtmlComments($html)
	{
		if($this->params->get('save_conditional_comments'))
		{
			return preg_replace("#<!--\s*(?:(?!-->|\[[^\]]+\]>).)*-->#s", '', $html);
		}
		else
		{
			return preg_replace("#<!--.*?-->#s", "", $html);
		}
	}
	
	
	PRIVATE FUNCTION _compressHtml($html)
	{
		$this->_bl_placeholder = "BL_SHTMLCMP_" . rand(0, 10000000);
		$this->_el_placeholder = "EL_SHTMLCMP_" . rand(0, 10000000);
		
		foreach($this->_exclusions as $exclusion)
		{
			if(is_array($exclusion))
			{
				if(!empty($exclusion[2]))
				{
					$exclusion[0] = preg_quote($exclusion[0], "#");
					$exclusion[1] = preg_quote($exclusion[1], "#");
				}
				
				$html = preg_replace_callback('#' . $exclusion[0] . '.*?' . $exclusion[1] . '#is', array($this, '_extractBlocks'), $html);
			
			}
			elseif($exclusion)
			{
				$exclusion = explode(', ', $exclusion);
				
				foreach($exclusion as &$element)
				{
					$element = '(' . preg_quote($element, '#') . ')';
				}
				
				$html = preg_replace_callback('#<(?|' . implode('|', $exclusion) . ')[^>]*>.+?</\1\s*>#is', array($this, '_extractElements'), $html);
			
			}
		}
		
		$html = preg_replace("#(?:(?:\r\n|\r|\n|\t)+| {2,})#", " ", $html);
		$html = preg_replace("#> +<#", "><", $html);
		
		if(!empty($this->_blocks))
		{
			$html = preg_replace_callback('#' . $this->_bl_placeholder . '#', array($this, '_restoreBlocks'), $html);
		}
		
		if(!empty($this->_elements))
		{
			$html = preg_replace_callback('#' . $this->_el_placeholder . '#', array($this, '_restoreElements'), $html);
		}
		
		return $html;
		
	}
	
	
	PRIVATE FUNCTION _extractBlocks($m)
	{
		$this->_blocks[] = $m[0];
		return $this->_bl_placeholder;
	}
	
	
	PRIVATE FUNCTION _extractElements($m)
	{
		$this->_elements[] = $m[0];
		return $this->_el_placeholder;
	}
	
	
	PRIVATE FUNCTION _restoreBlocks($m)
	{
		return array_shift($this->_blocks);
	}
	
	
	PRIVATE FUNCTION _restoreElements($m)
	{
		return array_shift($this->_elements);
	}
	
	
	PRIVATE FUNCTION _countScriptExecutionTime($html = null)
	{
		static $result = array(), $format = 5;
		
		$backtrace = debug_backtrace();
		$caller = $backtrace[1]['function'];
		
		$result[$caller] = !isset($result[$caller]) ? microtime(true) : 
			number_format((microtime(true) - $result[$caller]), $format);
		
		if($html)
		{
			$total = number_format((array_sum($result)), $format);
			$notice = __CLASS__ . ' (PHP script execution time):';
			
			if($this->_scriptExecutionTime == 1)
			{
				$notice = "\\n {$notice}";
				foreach($result as $name => $time)
				{
					$notice .= "\\n $name: $time sec.";
				}	
				$notice .= "\\n Total execution time: {$total} sec.";
				return preg_replace('#</head>#', "<script>if('console' in window && console.log) console.log('$notice')</script>$0", $html);
			}
			else
			{
				$notice = "<div><p style='margin: 15px; text-align: left'>{$notice}";
				foreach($result as $name => $time)
				{
					$notice .= "<br>$name: $time sec.";
				}	
				$notice .= "<br>total execution time: <b>{$total} sec.</b></div>";
				return preg_replace('#<body[^>]*>#', "$0{$notice}", $html);
			}
		}
		
	}
	
	
}