<?php
/**
 * @version        1.0
 * @package        DJ Img To Webp
 * @copyright    Copyright (C) 2019 DJ-Extensions.com LTD, All rights reserved.
 * @license        http://www.gnu.org/licenses GNU/GPL
 * @author        url: http://design-joomla.eu
 * @author        email contact@design-joomla.eu
 * @developer    Mateusz Maciejewski - mateusz.maciejewski@indicoweb.com
 *
 * You should have received a copy of the GNU General Public License.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

defined('_JEXEC') or die();
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

use Joomla\Registry\Registry;

defined('_JEXEC') or die();
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');


class JFormFieldDJWebpSupport extends JFormField
{

    protected $type = 'DJWebpSupport';



    public function renderField($options = array())
    {
        $layout = new JLayoutFile('webp_support', JPATH_ROOT . '/plugins/system/djimgtowebp/layouts');
        return $layout->render();
    }



}

?>