<?php
/**
 * @version        1.0
 * @package        DJ Img To Webp
 * @copyright    Copyright (C) 2019 DJ-Extensions.com LTD, All rights reserved.
 * @license        http://www.gnu.org/licenses GNU/GPL
 * @author        url: http://design-joomla.eu
 * @author        email contact@design-joomla.eu
 * @developer    Mateusz Maciejewski - mateusz.maciejewski@indicoweb.com
 *
 * You should have received a copy of the GNU General Public License.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

defined('_JEXEC') or die();
defined('JPATH_BASE') or die;

$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::root() . '/media/jui/css/icomoon.css');

$gdInfo = gd_info();
$serverSupportWebp = (isset($gdInfo['WebP Support']) && $gdInfo['WebP Support']) ? true : false;
$browserSupportWebp = (!(strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' )) || strpos( $_SERVER['HTTP_USER_AGENT'], ' Chrome/' ) !== false) ? true : false;

require_once JPATH_ROOT.'/plugins/system/djimgtowebp/helper.php';

?>

<div class="webpSupportContainer">
    <h3><?php echo JText::_('PLG_SYSTEM_DJIMGTOWEBP_SUPPORT_TABLE') ?></h3>
    <table>
        <thead>
        <th><?php echo JText::_('PLG_SYSTEM_DJIMGTOWEBP_SUPPORT_ENDPOINT'); ?></th>
        <th><?php echo JText::_('PLG_SYSTEM_DJIMGTOWEBP_SUPPORT_SUPPORTED'); ?></th>
        </thead>
        <tbody>
        <tr>
            <td><?php echo JText::_('PLG_SYSTEM_DJIMGTOWEBP_SUPPORT_SERVER'); ?></td>
            <td>

                <?php if ($serverSupportWebp) : ?>
                    <span class="icon-publish">&nbsp;</span>
                <?php else : ?>
                    <span class="icon-unpublish">&nbsp;</span></td>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td><?php echo JText::_('PLG_SYSTEM_DJIMGTOWEBP_SUPPORT_BROWSER'); ?></td>
            <td>
                <?php if (DJWebPHelper::browserSupportWebp()) : ?>
                    <span class="icon-publish">&nbsp;</span>
                <?php else : ?>
                <span class="icon-unpublish">&nbsp;</span></td>
            <?php endif; ?>
            </td>
        </tr>
        </tbody>
    </table>

</div>
<div style="clear: both"></div>
<style>
    .webpSupportContainer table {
        width: 200px;
        text-align: center;
    }

    .webpSupportContainer table th {
        width: 50%;
    }

    .webpSupportContainer table td,.webpSupportContainer table th {
        border: 1px solid black;
        width: 50%;
    }

    .webpSupportContainer {
        margin-bottom: 20px;
    }

</style>