<?php
	defined('_JEXEC') or die('Restricted access');
	$currency = CurrencyDisplay::getInstance();
	if (empty($this->product)) {
		echo vmText::_('COM_VIRTUEMART_PRODUCT_NOT_FOUND');
		echo '<br /><br />  ' . $this->continue_link_html;
		
		return;
	}
	
	echo shopFunctionsF::renderVmSubLayout('askrecomjs', array('product' => $this->product));
	
	if (vRequest::getInt('print', false))
	{
?>
<body onload="javascript:print();">
<?php
	}
?>

<div class="productdetails-view productdetails">
	<div class="vm-product-container">
		<div class="col-sm-12 col-md-8 vm-product-images-container">
			<?php echo $this->loadTemplate('images'); ?>
		</div>

		<div class="col-sm-12 col-md-4 vm-product-details-container">
			<h1 class="product_title" itemprop="name"><?php echo $this->product->product_name; ?></h1>
			<?php echo $this->product->event->afterDisplayTitle ?>
			<div class="product-short-description">
				<h3 style="color: #BFBFC1;font-size: 19px;">SKU:<?php echo $this->product->product_sku; ?></h3>
			</div>
			<?php echo shopFunctionsF::renderVmSubLayout('prices2', array('product' => $this->product, 'currency' => $this->currency)); ?>
			<div class="clearfix"></div>
			<?php
				if (!empty($this->product->prices['product_price'])) {
					
					?>
					<div class="product-short-description">
						<h5 style="color: #BFBFC1;">Tiết Kiệm:
							<div class="savemoney">
								<?php
									if ($this->product->prices['product_price'] - $this->product->prices['salesPrice'] > 0) {
										echo '<div class="PricesalesPrice vm-display vm-price-value"><span class="PricesalesPrice percent"> - ';
										echo (($this->product->prices['product_price'] - $this->product->prices['salesPrice']) / $this->product->prices['product_price']) * 100;
										echo ' % </span></div>';
										echo $currency->createPriceDiv('discountAmount', 'COM_VIRTUEMART_PRODUCT_DISCOUNT_AMOUNT', $this->product->prices);
									}
								?>
							</div>
						</h5>
					</div>
					<?php
				}
			?>
			<div class="product-short-description">
				<h5 style="color: #BFBFC1;">Giá Thị
					Trường:<?= $currency->createPriceDiv('product_price', '', $this->product->prices, FALSE, FALSE, 1.0, TRUE); ?></h5>
			</div>
			
			<?php
				if (!empty($this->product->product_s_desc)) {
					?>
					<div class="product-short-description">
						<?php echo nl2br($this->product->product_s_desc); ?>
					</div>
					<?php
				}
			?>
			<?php
				echo shopFunctionsF::renderVmSubLayout('addtocart', array('product' => $this->product));
				
				echo shopFunctionsF::renderVmSubLayout('productdetailservice', array());
				
				echo shopFunctionsF::renderVmSubLayout('call_api_boxme_fee', array('product' => $this->product, 'currency' => $this->currency));
			?>
			<div class="clearfix"></div>


			<div class="product-other-container">
				<div class="display_payment">
					<?php echo shopFunctionsF::renderVmSubLayout('product_buy_together', array('product' => $this->product, 'currency' => $this->currency)); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="vm-product-container">
		<div class="col-md-12 product-tabs">
			<?php
				echo shopFunctionsF::renderVmSubLayout('productdetailservicetabcsn', array());
			?>
		</div>
	</div>

	<div class="vm-product-container">
		<div class="col-md-12 product-tabs">
			<!-- Nav tabs -->
			<ul id="addreview" class="nav nav-tabs clearfix" role="tablist">
				<li role="presentation" class="clearfix active"><a href="#desc" aria-controls="desc" role="tab"
																   data-toggle="tab"><?php echo $this->product->product_name; ?></a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content clearfix">
				<div role="tabpanel" class="tab-pane clearfix active" id="desc">

                    <div class="product-description" id="descmobile">
						<?php echo substr($this->product->product_desc,0,1000); ?>
                        <span id="dots">...</span>
                        <div id="more" style="display: none">
                            <?php echo substr($this->product->product_desc,1000); ?>
                        </div>
                    </div>

                    <button onclick="descmobile()" id="myBtn">Read more</button>
                    <div class="clearfix"></div>
                    <script>
                        function descmobile() {
                            var dots = document.getElementById("dots");
                            var moreText = document.getElementById("more");
                            var btnText = document.getElementById("myBtn");

                            if (dots.style.display === "none") {
                                dots.style.display = "inline";
                                btnText.innerHTML = "Read more";
                                moreText.style.display = "none";
                            } else {
                                dots.style.display = "none";
                                btnText.innerHTML = "Read less";
                                moreText.style.display = "inline";
                            }
                        }
                    </script>
                    
                    
                    
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="vm-product-container">
		<div class="col-md-12 product-tabs">
			<!-- Nav tabs -->
			<ul id="addreview" class="nav nav-tabs clearfix" role="tablist">
				<li role="presentation" class="clearfix active"><a href="#reviews" aria-controls="reviews" role="tab"
																   data-toggle="tab"><?php echo vmText::_('Nhận Xét Sản Phẩm') ?></a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content clearfix">
				<div role="tabpanel" class="tab-pane clearfix active" id="reviews">
					<?php
						echo $this->product->event->afterDisplayContent;
						echo $this->loadTemplate('reviews');
					?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="col-md-12 product-related">
		<?php echo shopFunctionsF::renderVmSubLayout('related_products_v2', array('product' => $this->product, 'position' => 'related_products', 'class' => 'product-related-products', 'customTitle' => true)); ?>
	</div>

</div>
<?php echo vmJsApi::writeJS();  ?>


