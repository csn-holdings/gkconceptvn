<?php
	defined('_JEXEC') or die('Restricted access');
	$document = JFactory::getDocument();
	$app = JFactory::getApplication();
	$templateDir = JURI::base() . 'templates/' . $app->getTemplate();
?>
    <div id="imagesdestop">
        <!-- GOOGLE WEB FONTS -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,400,500,700,900&amp;ver=1.0.0"
              type="text/css" media="all"/>

        <!-- BOOTSTRAP 3.3.7 CSS -->
        <link rel="stylesheet" href="templates/sj_shoppystore/css/theme/bootstrap.min.css"/>

        <!-- OPEN LIBS CSS -->
        <link rel="stylesheet" href="templates/sj_shoppystore/css/theme/font-awesome.min.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/owl-carousel/owl.carousel.min.css"/>

        <link rel="stylesheet" href="templates/sj_shoppystore/css/js_composer/js_composer.min.css"/>

        <!-- YT CSS -->
        <link rel="stylesheet" href="templates/sj_shoppystore/css/colorbox/colorbox.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/sw_core/jquery.fancybox.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/sw_woocommerce/slider.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/woocommerce/woocommerce-layout.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/woocommerce/woocommerce-smallscreen.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/woocommerce/woocommerce.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/theme/wishtlist.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/theme/app-responsive.min.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/theme/flexslider.min.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/theme/custom-violet.min.css"/>
        <link rel="stylesheet" href="templates/sj_shoppystore/css/theme/home-style-14.min.css"/>
        <script type="text/javascript" src="templates/sj_shoppystore/js/sw_woocommerce/slick.min.js"></script>
        <script type="text/javascript" src="plugins/system/ytshortcodes/assets/js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="templates/sj_shoppystore/js/prettyPhoto/jquery.prettyPhoto.init.min.js"></script>
        <style>
            .size-full {
                width: 100%;
                height: auto;
                margin: 0 0 30px 0;
            }
        </style>
	    <?php
		    if (!empty($this->product->images) and count($this->product->images) > 1) { ?>
                <div class="portfolio-product-wrapper">
                    <ul style="list-style: none;" id="container_sw_portfolio_product_01"
                        class="portfolio-product-content portfolio-product-grid clearfix">
					    <?php
						    // List all Images
						    if (count($this->product->images) > 0)
						    {
							    foreach ($this->product->images as $key => $image) {
								    if ($key >= 6) {
									    break;
								    }
								    $imageslarge = YTTemplateUtils::resize($image->file_url, '600', '600', 'fill','webp');
								    $imagesradditional = YTTemplateUtils::resize($image->file_url, '1000', '1000', 'fill','webp');
								    ?>
                                    <li class="portfolio-product-item col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <div class="item-wrap" class="product-images" data-rtl="false" data-vertical="false">
                                            <div class="item-img products-thumb">
                                                <a href="#">
                                                    <div class="item-img-slider">
                                                        <div class="images">
                                                            <a href="<?php echo JURI::root() . $image->file_url; ?>" rel="prettyPhoto[product-gallery]" class="zoom">
                                                                <img 	width="600" height="600" class="attachment-shop_single size-full size-shop_single" alt=""
                                                                        src="<?php echo $image->file_url; ?>"
                                                                        srcset="<?php echo $image->file_url; ?> 600w, <?php echo $image->file_url; ?> 260w, <?php echo $image->file_url; ?> 180w, <?php echo $image->file_url; ?> 300w"
                                                                        sizes="(max-width: 600px) 100vw, 600px"
                                                                />
                                                            </a>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
							    <?php }
						    }
					    ?>
                    </ul>
                </div>
		
		    <?php }
	    ?>
    </div>
    <div id="imagesmobile">
	    <?php
		    if (!empty($this->product->images) and count ($this->product->images)>1) {   ?>
                <div id="thumb-slider" class="thumb-vertical-outer">
                    <div id="thumb-slider-next"><i class="fa fa-angle-up"></i></div>
                    <ul class="thumb-vertical">
					    <?php
						    if (count($this->product->images) > 0) {
							    foreach ($this->product->images as $key=>$image)
							    {
								    ?>
                                    <li class="owl2-item">
                                        <a href="#" class="img thumbnail" data-image="<?php echo JURI::base() . $image->file_url;?>" data-zoom-image="<?php echo JURI::base() . $image->file_url;?>"  >
                                            <img src="<?php echo JURI::base() . $image->file_url;?>" alt="" />
                                        </a>
                                    </li>
							    <?php
							    }
						    }
					    ?>
                    </ul>
                    <div id="thumb-slider-prev"><i class="fa fa-angle-down"></i></div>
                </div>
		
		    <?php }
		
		    // Product Main Image
		    if (!empty($this->product->images[0]))
		    {
			    ?>
                <div class="main-images">
                    <div class="large-image">
                        <img id="zoom_img_large" itemprop="image" class="product-image-zoom" data-zoom-image="<?php echo JURI::base() . $this->product->images[0]->file_url;?>" src="<?php echo JURI::base() . $this->product->images[0]->file_url;?>" title="" alt="" />
                    </div>
                    <span id="zimgex"><i class="fa fa-search-plus"></i></span>
                </div>

                <div class="main-images-quickview">
                    <img src="<?php echo JURI::base() . $this->product->images[0]->file_url;?>" title="" alt="" />
                </div>
		
		    <?php } ?>
	    <?php
		    $document = JFactory::getDocument();
		    $app = JFactory::getApplication();
		    $templateDir = JURI::base() . 'templates/' . $app->getTemplate();
	    ?>
        <script type="text/javascript" src="<?php echo $templateDir.'/js/jquery.elevateZoom-3.0.8.min.js' ?>"></script>
        <script type="text/javascript" src="<?php echo $templateDir.'/js/lightslider/lightslider.js' ?>"></script>
        <link rel="stylesheet" href="<?php echo $templateDir.'/js/lightslider/lightslider.css'?>" type="text/css">

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                var zoomCollection = '.large-image img';
                $(zoomCollection).elevateZoom({
                    gallery:'thumb-slider',
                    galleryActiveClass: "active",
                    zoomType    : "inner",
                    cursor: "crosshair",
                    easing:true
                });
                $("#zimgex").bind("click", function(e) {
                    var ez = $('#zoom_img_large').data('elevateZoom');
                    $._fancybox(ez.getGalleryList());
                    return false;
                });

                var _isMobile = {
                    iOS: function() {

                        return navigator.userAgent.match(/iPhone/i);
                    },
                    any: function() {
                        return (_isMobile.iOS());
                    }
                };


                var thumbslider = $(".thumb-vertical-outer .thumb-vertical").lightSlider({
                    item: 4,
                    autoWidth: false,
                    vertical:true,
                    slideMargin: 0,
                    verticalHeight:440,
                    pager: false,
                    controls: false,
                    //rtl: true,
                    prevHtml: '<i class="fa fa-angle-up"></i>',
                    nextHtml: '<i class="fa fa-angle-down"></i>',
                    responsive: [
                        {
                            breakpoint: 1199,
                            settings: {
                                verticalHeight: 300,
                                item: 3,
                            }
                        },
                        {
                            breakpoint: 1024,
                            settings: {
                                verticalHeight: 400,
                                item: 4,
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                verticalHeight: 360,
                                item: 3,
                            }
                        },
                        {
                            breakpoint: 567,
                            settings: {
                                verticalHeight: 210,
                                item: 2,
                            }
                        },
                        {
                            breakpoint: 360,
                            settings: {
                                verticalHeight: 100,
                                item: 1,
                            }
                        }

                    ]

                });
                $('#thumb-slider-prev').click(function(){
                    thumbslider.goToPrevSlide();
                });
                $('#thumb-slider-next').click(function(){
                    thumbslider.goToNextSlide();
                });
            });
        </script>
    </div>



