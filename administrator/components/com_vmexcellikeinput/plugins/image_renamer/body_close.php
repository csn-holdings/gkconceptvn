<div id="image-renamer-settings-panel" class="settings-panel pelmplugin_image_renamer" style="display:none;">

	<form style="text-align:center;" method="post" class="plem-form" >
		<h1>Image Renamer</h1>
		<br/>
		<label><?php echo pelm_sprintf("Number of image name columns");?></label> 
		<input type="numeric" id="ir_images_count" name="images" value="<?php echo $SETTINGS->image_renamer->images ?>" /> 
		<br/>
		<label><?php echo pelm_sprintf("Use in export");?></label> 
		<input type="checkbox" id="ir_images_use_in_export" name="use_in_export" value="1" <?php echo $SETTINGS->image_renamer->use_in_export ? ' checked="checked" ' : ''?> /> 
		<br/>
		<label><?php echo pelm_sprintf("Use in import");?></label> 
		<input type="checkbox" id="ir_images_use_in_import" name="use_in_import" value="1" <?php echo $SETTINGS->image_renamer->use_in_import ? ' checked="checked" ' : ''?> /> 
		<br/>
		<label><?php echo pelm_sprintf("Control thumbs names");?></label> 
		<input type="checkbox" id="ir_images_thumbs_names" name="thumbs_names" value="1" <?php echo $SETTINGS->image_renamer->thumbs_names ? ' checked="checked" ' : ''?> /> 
		<br/>
		<label><?php echo pelm_sprintf("If image with new name already exists overwrite it!");?></label> 
		<input type="checkbox" id="ir_images_overwrite" name="overwrite" value="1" <?php echo $SETTINGS->image_renamer->overwrite ? ' checked="checked" ' : ''?> /> 
		<br/>
		<span  style="color:red;">USE IMPORT WITH CAUTION!</span>
		<br/>
		<script data-cfasync="false" type="text/javascript">
			function ir_settings_save(){
				jQuery.ajax({
					url: window.location.href + "&image_renamer_save=1",
					type: "POST",
					dataType: "json",
					data: JSON.stringify( {
						images : parseInt(jQuery("#ir_images_count").val()),
						use_in_export :	jQuery("#ir_images_use_in_export").prop('checked') ? 1 : 0,					
						use_in_import : jQuery("#ir_images_use_in_import").prop('checked') ? 1 : 0,
						thumbs_names:  jQuery("#ir_images_thumbs_names").prop('checked') ? 1 : 0,
						overwrite : jQuery("#ir_images_overwrite").prop('checked') ? 1 : 0
					}),
					success: function (returned_data) {
						doLoad();
					},
					error: function(a,b,c){
						alert( "ERROR SAVING SETTINGS!");
					}
				});
			}
		</script>
		
		<button id="cmdIRSettingsCancel" ><?php echo JText::sprintf("COM_VMEXCELLIKEINPUT_IMPORT_CANCEL"); ?></button>
		<button id="cmdIRSettingsSave" ><?php echo JText::sprintf("COM_VMEXCELLIKEINPUT_SAVE"); ?></button>	
	</form>
</div>

<script data-cfasync="false" type="text/javascript">
 jQuery(document).on("click",'#cmdIRSettingsSave',function(e){
	e.preventDefault();
	ir_settings_save();
 });
 
 jQuery(document).on("click",'#cmdIRSettingsCancel',function(e){
	e.preventDefault();
	jQuery("#image-renamer-settings-panel").hide();
 });
 
</script>
