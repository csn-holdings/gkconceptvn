<?php

/**
 * @package         Convert Forms
 * @version         2.7.1 Pro
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2020 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

namespace ConvertForms\Field;

defined('_JEXEC') or die('Restricted access');

class Country extends \ConvertForms\FieldChoice
{
	protected $inheritInputLayout = 'dropdown';

	/**
	 *  Set the field choices
	 *
	 *  @return  array  The field choices array
	 */
    protected function getChoices()
    {	
    	// Get list of all countries
    	$countries = \NRFramework\Countries::getCountriesList();
		asort($countries);

		$choices = array();

		// Exclude countries
		if (!empty($this->field->exclude_countries))
		{
			$countries_ex = explode(',', $this->field->exclude_countries);

			if (is_array($countries_ex))
			{
				foreach ($countries_ex as $country)
				{
					unset($countries[strtoupper(trim($country))]);
				}
			}
		}

		// Detect visitor's country
		if ($this->field->detectcountry && $visitorCountryCode = $this->getVisitorCountry())
		{
			$this->field->value = $visitorCountryCode;
		}

		foreach ($countries as $countryCode => $countryName)
		{
			$choices[] = array(
				'value'    => $this->field->save == 'name' ? $countryName : $countryCode,
				'label'    => $countryName,
				'selected' => in_array(strtolower($this->field->value), array(strtolower($countryCode), strtolower($countryName)))
			);
		}
		
		// If we have a placeholder available, add it to dropdown choices.
        if (isset($this->field->placeholder) && !empty($this->field->placeholder))
        {
            array_unshift($choices, array(
                'label'    => trim($this->field->placeholder),
                'value'    => '',
                'selected' => true,
                'disabled' => true
            ));
        }

		return $choices;
    }

    /**
     *  Detect visitor's country
     *
     *  @return  string   The visitor's country code (GR)
     */
    private function getVisitorCountry()
    {
    	$path = JPATH_PLUGINS . '/system/tgeoip/';

    	if (!\JFolder::exists($path))
    	{
    		return;
    	}

    	if (!class_exists('TGeoIP'))
    	{
        	@include_once $path . 'vendor/autoload.php';
        	@include_once $path . 'helper/tgeoip.php';
		}
		
		$geo = new \TGeoIP();
		
		return $this->field->save == 'name' ? $geo->getCountryName() : $geo->getCountryCode();
    }
}

?>